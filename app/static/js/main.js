// Require.js allows us to configure shortcut alias
require.config({
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		modernizr: {
			exports: 'Modernizr'
		},
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		},
		backboneLocalstorage: {
			deps: ['backbone'],
			exports: 'Store'
		},
		foundation: {
			deps: [
			       'jquery'
			       ],
			exports: 'Foundation'
		}
	},
	paths: {
		jquery: 'lib/jquery',
		underscore: 'lib/underscore',
		backbone: 'lib/backbone/backbone',
		backboneLocalstorage: 'lib/backbone/backbone.localStorage',
		text: 'lib/require/text',
		
		// foundation files
		modernizr:	'../bower_components/modernizr/modernizr',
		foundation:	'../bower_components/foundation/js/foundation.min'
		
	}
});

require([
    'modernizr',    
    'foundation',
    'jquery',
	'views/app',
	'routers/router'
], function( Modernizr, foundation_lib, $, AppView, Workspace ) {
	// Initialize routing and start Backbone.history()
	new Workspace();
	Backbone.history.start();

	// Initialize the application view
	new AppView();
	
	$(document).foundation();
});
