watch for changes in sass:
bundle exec compass watch

see:
* http://foundation.zurb.com/docs/sass.html

combine and minify the js files - create the dist:
* manage.py collectstatic --noinput
from the static dir, execute:
* node r.js -o build.js

see:
* http://backbonejs.org/
* http://www.webdeveasy.com/optimize-requirejs-projects/
* http://backbonetutorials.com/organizing-backbone-using-modules/